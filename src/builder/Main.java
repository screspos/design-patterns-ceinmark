package builder;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Pizza pizza = null;
		Cocina cocina = new Cocina();
		Scanner scan = new Scanner(System.in);
		System.out.println("�Qu� tipo de pizza desea usted?"
				+ "\n1=Margarita\t2=Carbonara\t3=Barbacoa");
		switch(scan.nextInt()) {
			case 1 -> pizza = cocina.cocinar(new MargaritaBuilder());
			case 2 -> pizza = cocina.cocinar(new CarbonaraBuilder());
			case 3 -> pizza = cocina.cocinar(new BarbacoaBuilder());
		}
		System.out.println("Usted va a comer: ");
		System.out.println(pizza);
		
	}
}
