package builder;

public abstract class PizzaBuilder {
	Pizza pizza;
	
	public PizzaBuilder() {
		pizza = new Pizza();
	}
	
	public abstract PizzaBuilder addBase(PizzaBuilder receta);
	
	public abstract PizzaBuilder addQueso(PizzaBuilder receta);
	
	public abstract PizzaBuilder addSalsa(PizzaBuilder receta);
	
	public abstract PizzaBuilder addIngredientes(PizzaBuilder receta);
	
	public Pizza entregar() {
		return this.pizza;
	}
}
