package builder;

public class MargaritaBuilder extends PizzaBuilder {

	public MargaritaBuilder() {
		super();
	}
	
	@Override
	public PizzaBuilder addBase(PizzaBuilder receta) {
		receta.pizza.setBase("Tomate");
		return receta;
	}

	@Override
	public PizzaBuilder addQueso(PizzaBuilder receta) {
		receta.pizza.setQueso("Mozzarella");
		return receta;
	}

	@Override
	public PizzaBuilder addSalsa(PizzaBuilder receta) {
		receta.pizza.setSalsa("Aceite");
		return receta;
	}

	@Override
	public PizzaBuilder addIngredientes(PizzaBuilder receta) {
		String[] ingredientes = {"Albahaca"};
		receta.pizza.setIngredientes(ingredientes);
		return receta;
	}
}
