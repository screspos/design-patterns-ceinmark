package builder;

public class CarbonaraBuilder extends PizzaBuilder {

	public CarbonaraBuilder() {
		super();
	}
	
	@Override
	public PizzaBuilder addBase(PizzaBuilder receta) {
		receta.pizza.setBase("Nata");
		return receta;
	}

	@Override
	public PizzaBuilder addQueso(PizzaBuilder receta) {
		receta.pizza.setQueso("Rayado");
		return receta;
	}

	@Override
	public PizzaBuilder addSalsa(PizzaBuilder receta) {
		receta.pizza.setSalsa("ninguna");
		return receta;
	}

	@Override
	public PizzaBuilder addIngredientes(PizzaBuilder receta) {
		String[] ingredientes = {"Cebolla","Champiņones","Bacon"};
		receta.pizza.setIngredientes(ingredientes);
		return receta;
	}
}
