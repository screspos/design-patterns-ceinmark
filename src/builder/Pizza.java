package builder;

import java.util.Arrays;

public class Pizza {
	private String base, queso, salsa;
	private String[] ingredientes;
	
	public Pizza() {
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getQueso() {
		return queso;
	}

	public void setQueso(String queso) {
		this.queso = queso;
	}

	public String getSalsa() {
		return salsa;
	}

	public void setSalsa(String salsa) {
		this.salsa = salsa;
	}

	public String[] getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(String[] ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	@Override
	public String toString() {
		return "Una deliciosa pizza con base de " + base +
		", queso " + queso + ", salsa " + salsa +
		" y los siguientes ingredientes:\n" +
		Arrays.toString(ingredientes);
	}
}