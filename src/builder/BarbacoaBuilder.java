package builder;

public class BarbacoaBuilder extends PizzaBuilder {

	public BarbacoaBuilder() {
		super();
	}
	
	@Override
	public PizzaBuilder addBase(PizzaBuilder receta) {
		receta.pizza.setBase("Tomate");
		return receta;
	}

	@Override
	public PizzaBuilder addQueso(PizzaBuilder receta) {
		receta.pizza.setQueso("Havarti y Mozzarella rallado");
		return receta;
	}

	@Override
	public PizzaBuilder addSalsa(PizzaBuilder receta) {
		receta.pizza.setSalsa("Barbacoa");
		return receta;
	}

	@Override
	public PizzaBuilder addIngredientes(PizzaBuilder receta) {
		String[] ingredientes = {"Carne picada","Pimiento rojo","Cebolleta","Tomate natural"};
		receta.pizza.setIngredientes(ingredientes);
		return receta;
	}
}