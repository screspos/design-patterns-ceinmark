package builder;

public class Cocina {
	public Pizza cocinar(PizzaBuilder receta) {
		// Dado que todos los m�todos de la clase
		//	PizzaBuilder retornan un objeto PizzaBuilder,
		//	estos m�todos se pueden encadenar para simplificar
		//	el c�digo e ir a�adiendo los diferentes ingredientes
		//  hasta que la pizza est� completa, y una vez completa,
		//  se entrega, devolviendo el nuevo objeto pizza.
		return receta.addIngredientes(
				receta.addSalsa(
				 receta.addQueso(
				  receta.addBase(receta)))
				).entregar();
	}
}