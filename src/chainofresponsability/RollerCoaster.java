package chainofresponsability;

public class RollerCoaster {
	// Espacio donde guardamos la primera validacion de la cadena
	private Validacion primera;
	
	RollerCoaster() {
		// Creamos la cadena de validaciones enlazadas
		primera = Validacion.enlazar(
				new ValidacionDNI(),
				new ValidacionAltura(),
				new ValidacionPeso(),
				new ValidacionEdad());
	}
	
	// Comienza la cadena de validaciones, empezando por la primera
	public boolean comprobar(Persona persona) {
		return primera.check(persona);
		// Al estar todas las validaciones enlazadas, seguir� la
		// cadena hasta el final. Si en la �ltima comprueba que la
		// siguiente es null, devolver� true. Si no pasa alguna de
		// ellas ir� devolviendo false de una en una.
	}
}
