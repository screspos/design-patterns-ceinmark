package chainofresponsability;

public class ValidacionEdad extends Validacion {

	@Override
	public boolean check(Persona persona) {
		return (persona.getEdad()>10)?checkNext(persona):false;
	}
}