package chainofresponsability;

public class Main {
	public static void main(String[] args) {
		RollerCoaster DragonKhan = new RollerCoaster();
		Persona adecuada = new Persona("Saul","12345678A",31,78.0f,1.74f);
		Persona noAdecuada = new Persona("Pancracio","12345678A",31,135.0f,1.74f);
		if (DragonKhan.comprobar(adecuada)) {
			System.out.println(adecuada.getNombre() + " se sube a la monta�a rusa.");
		} else {
			System.out.println(adecuada.getNombre() + " no se puede subir a la monta�a rusa.");
		}
		
		if (DragonKhan.comprobar(noAdecuada)) {
			System.out.println(noAdecuada.getNombre() + " se sube a la monta�a rusa.");
		} else {
			System.out.println(noAdecuada.getNombre() + " no se puede subir a la monta�a rusa.");
		}
	}
}
