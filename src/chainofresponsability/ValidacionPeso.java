package chainofresponsability;

public class ValidacionPeso extends Validacion {

	@Override
	public boolean check(Persona persona) {
		return (persona.getPeso() > 45 &&
				persona.getPeso() <120)?
				checkNext(persona):false;
	}

}
