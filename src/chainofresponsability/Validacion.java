package chainofresponsability;

public abstract class Validacion {
	// La direcci�n de memoria donde se guarda la siguiente validaci�n
	protected Validacion next;
	
	public static Validacion enlazar(Validacion first, Validacion... cadena) {
		Validacion head = first; // Asigno la primera validaci�n a la variable head
		for (Validacion siguiente:cadena) { // Por todos los elementos de la cadena
			head.next = siguiente; // Le asigno un elemento siguiente al head actual
			head = siguiente; // Meto el siguiente elemento de la cabeza en head
		}
		return first; // Devuelvo el primer elemento de la cadena, ya enlazado con
		// los dem�s.
	}
	
	// La comprobaci�n que hace cada Validaci�n de la cadena, diferente en cada una
	protected abstract boolean check(Persona persona);
	
	// Comprueba si quedan objetos en la cadena, y si no, devuelve true
	public boolean checkNext(Persona persona) {
		if (this.next==null) { // Si el objeto siguiente a este no existe...
			return true; // devuelve true
		}
		// si no...
		return next.check(persona); // Le pasa el turno a la siguiente Validaci�n de la cadena.
	}
}
