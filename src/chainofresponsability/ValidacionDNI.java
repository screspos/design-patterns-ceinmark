package chainofresponsability;

public class ValidacionDNI extends Validacion {

	@Override
	public boolean check(Persona persona) {
		return(persona.getDNI().length()==9)?
				checkNext(persona):false;
	}
}