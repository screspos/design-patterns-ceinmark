package chainofresponsability;

public class ValidacionAltura extends Validacion {

	@Override
	public boolean check(Persona persona) {
	return (persona.getAltura() > 1.20 &&
			persona.getAltura() < 2.10)?
			checkNext(persona):false;
	}

}
