package chainofresponsability;

public class Persona {
	private String nombre, DNI;
	private int edad;
	private float peso;
	private float altura;
	
	
	public Persona(String nombre, String dNI, int edad, float peso, float altura) {
		this.nombre = nombre;
		DNI = dNI;
		this.edad = edad;
		this.peso = peso;
		this.altura = altura;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	public float getAltura() {
		return altura;
	}
	public void setAltura(float altura) {
		this.altura = altura;
	}
}