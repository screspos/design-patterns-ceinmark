package singleton;

public class Main {
	public static void main(String[] args) {
		System.out.println(Singleton_1.getNombreAplicacion());
		// Las dos veces que intentamos acceder a la instancia del Singleton_2 nos devuelven la misma direcci�n de memoria.
		System.out.println(Singleton_2.getInstance());
		System.out.println(Singleton_2.getInstance());
	}
}