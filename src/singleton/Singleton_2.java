package singleton;

public class Singleton_2 {
	// El atributo donde guardamos la instancia
	private static Singleton_2 instancia;
	
	// El constructor privado
	private Singleton_2() {
	}
	
	// El m�todo para obtener la instancia
	public static Singleton_2 getInstance() {
		if (instancia==null) { // Si a�n no se ha creado una instancia...
			instancia = new Singleton_2(); // Creamos una.
		}
		return instancia; // Devolvemos la �nica instancia de esta clase.
	}
	// Resto del c�digo de la clase
}