package singleton;

public class Singleton_1 {
	// Atributo que usamos para guardar la instancia
	public static final Singleton_1 instancia = new Singleton_1();
	
	// Otros atributos:
	private String nombreAplicacion;
	private int numero;
	
	// Constructor privado:
	private Singleton_1() {
		// Creamos el objeto:
		nombreAplicacion="Mi aplicaci�n";
		numero = 0;
	}
	
	// Resto del c�digo de la clase. Los m�todos getter y setter devuelven y modifican los datos de la instancia.
	public static String getNombreAplicacion() {return instancia.nombreAplicacion;}
	public static void setNombreAplicacion(String nombreAplicacion) {instancia.nombreAplicacion = nombreAplicacion;}
	public static int getNumero() {return instancia.numero;}
	public static void setNumero(int numero) {instancia.numero = numero;}
}