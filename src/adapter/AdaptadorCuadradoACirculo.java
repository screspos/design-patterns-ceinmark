package adapter;

public class AdaptadorCuadradoACirculo extends FormaCircular {
	private FormaCuadrada cuadrado;
	
	public AdaptadorCuadradoACirculo(float radio) {
		super(radio);
		setCuadrado(radio);
	}
	
	public AdaptadorCuadradoACirculo(FormaCuadrada cuadrado) {
		this.cuadrado = cuadrado;
		setRadio(cuadrado);
	}
	
	private void setRadio(FormaCuadrada cuadrado) {
		this.radio = (float) (cuadrado.getLado() * Math.sqrt(2) / 2);
	}
	
	public FormaCuadrada getCuadrado() {
		return cuadrado;
	}
	
	public void setCuadrado(float radio) {
		cuadrado = new FormaCuadrada( radio * 2 /  (float) Math.sqrt(2));
	}
}