package adapter;

public class Main {
	public static void main(String[] args) {
		AreaCircular area = new AreaCircular(20.0f);
		FormaCircular circuloMenor = new FormaCircular(10.0f);
		FormaCircular circuloMayor = new FormaCircular(30.0f);
		FormaCuadrada cuadradoMenor = new FormaCuadrada(10.0f);
		FormaCuadrada cuadradoMayor = new FormaCuadrada(30.0f);
		
		System.out.print("Comprobando si c�rculo peque�o cabe:\t");
		System.out.println(area.puedeContener(circuloMenor));
		System.out.print("Comprobando si c�rculo grande cabe:\t");
		System.out.println(area.puedeContener(circuloMayor));
		System.out.print("Comprobando si cuadrado peque�o cabe:\t");
		System.out.println(area.puedeContener(new AdaptadorCuadradoACirculo(cuadradoMenor)));
		System.out.print("Comprobando si cuadrado grande cabe:\t");
		System.out.println(area.puedeContener(new AdaptadorCuadradoACirculo(cuadradoMayor)));
	}
}