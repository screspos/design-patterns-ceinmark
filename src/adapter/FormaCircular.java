package adapter;

public class FormaCircular {
	protected float radio;
	
	public FormaCircular() {
		
	}

	public FormaCircular(float radio) {
		this.radio = radio;
	}
	
	public float getRadio() {
		return radio;
	}
}