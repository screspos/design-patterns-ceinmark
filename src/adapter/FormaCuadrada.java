package adapter;

public class FormaCuadrada {
	private float lado;

	FormaCuadrada(float lado) {
		this.lado = lado;
	}
	
	public float getLado() {
		return lado;
	}
}