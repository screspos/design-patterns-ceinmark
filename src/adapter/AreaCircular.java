package adapter;

public class AreaCircular extends FormaCircular {

	public AreaCircular() {
		super();
	}

	public AreaCircular(float radio) {
		super(radio);
	}
	
	public boolean puedeContener(FormaCircular circulo) {
		return radio > circulo.getRadio();
	}
}
