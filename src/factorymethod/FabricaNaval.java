package factorymethod;

public class FabricaNaval extends Factory {

	@Override
	public Transporte crearTransporte() {
		return new Barco();
	}
}