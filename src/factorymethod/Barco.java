package factorymethod;

public class Barco implements Transporte {

	@Override
	public String transportar() {
		return "El barco transporta el paquete por mar.";
	}

}
