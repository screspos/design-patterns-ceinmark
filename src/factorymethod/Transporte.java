package factorymethod;

public interface Transporte {
	public String transportar();
}