package factorymethod;

public class FabricaTerrestre extends Factory {

	@Override
	public Transporte crearTransporte() {
		return new Camion();
	}	
}
