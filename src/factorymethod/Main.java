package factorymethod;

public class Main {
	public static void main(String[] args) {
		// Instanciamos las dos clases factory concretas.
		Factory[] fabricas = {new FabricaTerrestre(), new FabricaNaval()};
		// Array de transportes
		Transporte[] transportes = new Transporte[2]; 
		// Ahora vamos a demostrar que el c�digo que escribiremos es exactamente
		// igual tanto para transportes terrestres como mar�timos:
		
		// Por cada tipo de f�brica, creamos el transporte correspondiente.
		for(int i = 0; i < fabricas.length; i++) {
			transportes[i] = fabricas[i].crearTransporte();
		}
		// Ejecutamos el mismo m�todo para los diferentes transportes.
		for(Transporte t: transportes) {
			System.out.println(t.transportar());
		}
	}
}